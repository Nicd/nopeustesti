#include "randomiser.hpp"

#include <Arduino.h>
#include "config.hpp"

Randomiser::Randomiser(uint32_t seed) { randomSeed(seed); }

int32_t Randomiser::get() const { return random(0, BUTTONS); }
