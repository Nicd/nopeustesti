#include <Arduino.h>
#include "debugger.hpp"
#include "randomiser.hpp"

#pragma once

enum class NextOperationType { NOOP, END, ADD_SCORE };

/**
 * Next operation to be run by the game manager when the current loop has ended.
 */
struct NextOperation {
  NextOperationType type;
  uint8_t scoreToAdd;
};

class BaseGame {
 protected:
  Randomiser* randomiser;

 public:
  BaseGame(Randomiser* randomiser);

  virtual NextOperation loop() = 0;
};
