#include <Arduino.h>
#include "config.hpp"

#pragma once

/*
 * Data for the YL3 2x4 7-segment display.
 *
 * More reading:
 * https://github.com/hennroja/raspberrypi-yl-3-demo/blob/master/demo-YL3.py
 * http://forum.arduino.cc/index.php?topic=211197.msg1944509#msg1944509
 * https://playground2014.wordpress.com/arduino/3461bs/
 */

namespace YL3 {
/**
 * Data for all the characters. The pin order for the segments is PGFEDCBA. See this image for
 * reference (P = DP):
 * https://upload.wikimedia.org/wikipedia/commons/e/ed/7_Segment_Display_with_Labeled_Segments.svg
 *
 * Low (0) bit means the segment is lit up.
 */
const byte CHARS[] = {
    0b10111111,  // -
    0b01111111,  // .
    0b11000000,  // 0
    0b11111001,  // 1
    0b10100100,  // 2
    0b10110000,  // 3
    0b10011001,  // 4
    0b10010010,  // 5
    0b10000010,  // 6
    0b11111000,  // 7
    0b10000000,  // 8
    0b10010000,  // 9
    0b10001000,  // A
    0b10000011,  // B
    0b11000110,  // C
    0b10100001,  // D
    0b10000110,  // E
    0b10001110,  // F
    0b10000010,  // G
    0b10001001,  // H
    0b11111001,  // I
    0b11110001,  // J
    0b10000111,  // K
    0b11000111,  // L
    0b11001000,  // M
    0b10101011,  // N
    0b11000000,  // O
    0b10001110,  // P
    0b10011000,  // Q
    0b10101111,  // R
    0b10010010,  // S
    0b10000111,  // T
    0b11000001,  // U
    0b11000001,  // V
    0b11000001,  // W
    0b10001001,  // X
    0b10011001,  // Y
    0b10100100,  // Z
};

/**
 * Indices to use to select digits on the display. This starts from the right and goes to the left
 * for easier indexing when aligning to the right.
 */
const byte INDICES[] = {0b10000000, 0b01000000, 0b00100000, 0b00010000,
                        0b00001000, 0b00000100, 0b00000010, 0b00000001};

/**
 * Index that refers to all positions.
 */
const byte ALL = 0b11111111;

/**
 * Empty byte to clear display.
 */
const byte EMPTY = 0b11111111;

/**
 * Get byte to send to YL3 for given character.
 */
byte char2byte(const char c);

/**
 * Begin drawing on the screen.
 */
void drawBegin();

/**
 * End drawing on the screen.
 */
void drawEnd();

/**
 * Initialise pins for display.
 */
void initDisplay();

/**
 * Clear display entirely.
 */
void clearDisplay();

/**
 * Draw character on the screen in given index.
 */
void drawChar(uint8_t index, const char c);

};  // namespace YL3
