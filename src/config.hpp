#include <Arduino.h>

#pragma once

/**
 * Set to true to enable debug prints (via serial console) and false to disable.
 */
const bool DEBUG = true;

/**
 * Amount of buttons in the game.
 */
const uint8_t BUTTONS = 4;

/**
 * Pins of the buttons, in order from left to right in the case. Leftmost will be 0, and number will
 * increase to the right.
 */
const uint8_t BUTTON_PINS[BUTTONS] = {A0, A1, A2, A3};

/**
 * Debounce delay, i.e. how long a button must be high or low before it is accepted, to reduce
 * spurious inputs.
 */
const uint8_t DEBOUNCE_DELAY = 20;

/**
 * Delay at start of game between beeps, in milliseconds.
 */
const uint16_t DELAY_START = 570;

/**
 * Function to decrement the given delay. Should return the new smaller delay.
 */
const auto DELAY_DECREMENT = [](uint16_t delay) -> uint16_t {
  double newDelay;
  if (delay > 399) {
    newDelay = delay * 0.993;
  } else if (delay > 326) {
    newDelay = delay * 0.996;
  } else if (delay > 192) {
    newDelay = delay * 0.9985;
  } else {
    newDelay = delay - 1;
  }

  return static_cast<uint16_t>(newDelay);
};

/**
 * Maximum amount of beeps you can be "behind" before the game is stopped.
 */
const uint8_t MAX_WAITING = 20;

/**
 * RCK (latch) pin of YL3 display.
 */
const uint8_t YL3_RCK = 2;

/**
 * SCK (clock) pin of YL3 display.
 */
const uint8_t YL3_SCK = 3;

/**
 * DIO (data) pin of YL3 display.
 */
const uint8_t YL3_DIO = 4;
