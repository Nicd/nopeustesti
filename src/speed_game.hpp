#include "base_game.hpp"
#include "config.hpp"
#include "randomiser.hpp"

#pragma once

/**
 * The default speed test game where you need to press the buttons with an ever increasing speed.
 */
class SpeedGame : public BaseGame {
  uint8_t waitingBeeps[MAX_WAITING];
  uint8_t waitingBeepsAmount;
  uint32_t lastBeepAt;
  uint16_t currentDelay;

 public:
  SpeedGame(Randomiser* randomiser);
  NextOperation loop();
  uint8_t getNewBeep() const;
};
