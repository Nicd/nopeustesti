#include "yl3.hpp"

#include "config.hpp"

byte YL3::char2byte(char c) {
  if (c == 45) {
    return CHARS[0];
  } else if (c == 46) {
    return CHARS[1];
  } else if (c >= 48 && c < 58) {
    return CHARS[c - 48 + 2];
  } else if (c >= 65 && c < 91) {
    return CHARS[c - 65 + 12];
  } else {
    return CHARS[0];
  }
}

void YL3::drawBegin() { digitalWrite(YL3_RCK, LOW); }

void YL3::drawEnd() { digitalWrite(YL3_RCK, HIGH); }

void YL3::initDisplay() {
  pinMode(YL3_RCK, OUTPUT);
  pinMode(YL3_SCK, OUTPUT);
  pinMode(YL3_DIO, OUTPUT);
}

void YL3::clearDisplay() {
  drawBegin();
  shiftOut(YL3_DIO, YL3_SCK, MSBFIRST, ALL);
  shiftOut(YL3_DIO, YL3_SCK, MSBFIRST, EMPTY);
  drawEnd();
}

void YL3::drawChar(const uint8_t index, const char c) {
  drawBegin();
  const auto b = char2byte(c);
  shiftOut(YL3_DIO, YL3_SCK, MSBFIRST, INDICES[index]);
  shiftOut(YL3_DIO, YL3_SCK, MSBFIRST, b);
  drawEnd();
}
