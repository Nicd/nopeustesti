#include "speed_game.hpp"

#include <Arduino.h>
#include "base_game.hpp"
#include "config.hpp"

SpeedGame::SpeedGame(Randomiser* randomiser)
    : BaseGame(randomiser), waitingBeeps{}, waitingBeepsAmount(0), currentDelay(DELAY_START) {
  lastBeepAt = millis();
}

NextOperation SpeedGame::loop() {
  const auto now = millis();

  if ((now - lastBeepAt) >= static_cast<uint32_t>(currentDelay)) {
    lastBeepAt = now;
    waitingBeeps[waitingBeepsAmount] = getNewBeep();
    ++waitingBeepsAmount;
    currentDelay = DELAY_DECREMENT(currentDelay);

    if (waitingBeepsAmount == MAX_WAITING) {
      return {NextOperationType::END, 0};
    } else {
      return {NextOperationType::ADD_SCORE, 1};
    }
  }

  return {NextOperationType::NOOP, 0};
}

uint8_t SpeedGame::getNewBeep() const {
  uint16_t beep = 0;
  do {
    beep = randomiser->get();
  } while (waitingBeepsAmount > 0 && beep == waitingBeeps[waitingBeepsAmount - 1]);

  return static_cast<uint8_t>(beep);
}
