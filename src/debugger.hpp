#include <Arduino.h>

#pragma once

void initDebug(uint16_t speed);
void debugPrint(const String msg, const bool newline = true);
