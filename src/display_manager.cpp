#include "display_manager.hpp"

#include <Arduino.h>
#include "yl3.hpp"

DisplayManager::DisplayManager() : currentIndex(0), currentDisplay(""), currentDisplayLength(0) {
  YL3::initDisplay();
}

void DisplayManager::clearDisplay() {
  YL3::clearDisplay();
  currentDisplay = "";
  currentIndex = 0;
  currentDisplayLength = 0;
}

void DisplayManager::drawText(const String text) {
  clearDisplay();

  const auto length = text.length();
  currentDisplay = text;
  currentDisplayLength = length;
  currentIndex = 0;
}

void DisplayManager::drawNumber(const uint32_t number) {
  const auto str = String(number);
  drawText(str);
}

void DisplayManager::loop() {
  if (currentDisplayLength == 0) {
    return;
  }

  const char c = currentDisplay.charAt(currentIndex);

  uint8_t i = currentIndex;
  if (currentDisplayLength > 8) {
    i = currentIndex - 8;
  }

  YL3::drawChar(currentDisplayLength - 1 - i, c);

  ++currentIndex;
  if (currentIndex >= currentDisplayLength) {
    currentIndex = 0;
  }
}
