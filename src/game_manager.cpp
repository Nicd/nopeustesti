#include "game_manager.hpp"

#include <Arduino.h>
#include "base_game.hpp"
#include "config.hpp"
#include "debugger.hpp"
#include "display_manager.hpp"
#include "input.hpp"
#include "randomiser.hpp"
#include "speed_game.hpp"

GameManager::GameManager(Input* input, Randomiser* randomiser, DisplayManager* displayManager)
    : input(input),
      randomiser(randomiser),
      displayManager(displayManager),
      state(State::GAME),
      currentScore(0) {
  debugPrint("Initialising new game...");
  currentGame = new SpeedGame(randomiser);
  displayManager->clearDisplay();
}

void GameManager::loop() {
  displayManager->loop();

  if (state == State::GAME) {
    auto nextOp = currentGame->loop();

    if (nextOp.type == NextOperationType::ADD_SCORE) {
      currentScore += nextOp.scoreToAdd;
      debugPrint("Added score!");
      displayManager->drawNumber(currentScore);
    } else if (nextOp.type == NextOperationType::END) {
      debugPrint("Game ended.");
      state = State::SCORE;
      displayManager->drawText("END");
    }
  }
}
