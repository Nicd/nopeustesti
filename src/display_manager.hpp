#include <Arduino.h>

#pragma once

/**
 * Display manager for the YL3 8x7-segment display.
 *
 * The YL3 can only display one digit at a time, in as many positions as needed. Usually though we
 * need to display many digits in different positions. This display manager will keep in memory the
 * displayable text and display one digit at a time, flickering the display fast enough so that the
 * user doesn't notice.
 */
class DisplayManager {
  uint8_t currentIndex;
  String currentDisplay;
  uint8_t currentDisplayLength;

 public:
  DisplayManager();

  /**
   * Clear display entirely.
   */
  void clearDisplay();

  /**
   * Draw text on the screen, right aligned. Text longer than 8 characters will be truncated.
   */
  void drawText(const String text);

  /**
   * Draw a number on the screen, right aligned. Numbers larger than can fit on the display will be
   * truncated.
   */
  void drawNumber(const uint32_t number);

  /**
   * Draw character and advance to next one for next loop.
   */
  void loop();
};
