#include "debugger.hpp"

#include <Arduino.h>
#include "config.hpp"

void initDebug(uint16_t speed) { Serial.begin(speed); }

void debugPrint(const String msg, const bool newline) {
  if (DEBUG) {
    auto msgCopy = String(msg);

    if (newline) {
      msgCopy.concat("\r\n");
    }

    Serial.write(msgCopy.c_str());
  }
}
