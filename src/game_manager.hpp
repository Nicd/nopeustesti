#include "base_game.hpp"
#include "config.hpp"
#include "display_manager.hpp"
#include "input.hpp"
#include "randomiser.hpp"

#pragma once

enum class State { MENU, INIT, GAME, SCORE };

class GameManager {
  Input* input;
  Randomiser* randomiser;
  DisplayManager* displayManager;
  State state;
  uint16_t currentScore;
  BaseGame* currentGame;

 public:
  GameManager(Input* input, Randomiser* randomiser, DisplayManager* displayManager);

  void loop();
};
