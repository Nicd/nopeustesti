#include <Arduino.h>

#pragma once

class Randomiser {
 public:
  Randomiser(uint32_t seed);
  int32_t get() const;
};
