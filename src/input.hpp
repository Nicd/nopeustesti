#include <Arduino.h>
#include "config.hpp"

#pragma once

enum class ButtonState { OFF, ON };

struct InputStatus {
  ButtonState buttons[BUTTONS];
};

/**
 * Class managing the input coming from buttons, and their debouncing. Also manages setting up the
 * proper pins.
 */
class Input {
  uint32_t lastChangeTimes[BUTTONS];
  InputStatus currentStates;
  InputStatus debouncedStates;

 public:
  Input();

  void process();
  const InputStatus& getStatus() const;
};
