#include "input.hpp"

#include <Arduino.h>
#include "config.hpp"

Input::Input() : lastChangeTimes{}, currentStates{}, debouncedStates{} {
  for (uint8_t i = 0; i < BUTTONS; ++i) {
    pinMode(BUTTON_PINS[i], INPUT_PULLUP);
  }
}

void Input::process() {
  const auto now = millis();
  for (uint8_t i = 0; i < BUTTONS; ++i) {
    // Note that LOW is ON, HIGH is OFF when using pullup
    const auto state = (digitalRead(BUTTON_PINS[i]) == LOW) ? ButtonState::ON : ButtonState::OFF;

    if (state != currentStates.buttons[i]) {
      currentStates.buttons[i] = state;
      lastChangeTimes[i] = now;
    } else if (now - lastChangeTimes[i] >= DEBOUNCE_DELAY && state != debouncedStates.buttons[i]) {
      debouncedStates.buttons[i] = state;
    }
  }
}

const InputStatus& Input::getStatus() const { return debouncedStates; }
