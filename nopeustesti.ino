#include "src/debugger.hpp"
#include "src/display_manager.hpp"
#include "src/game_manager.hpp"
#include "src/randomiser.hpp"

Input* input;
Randomiser* randomiser;
GameManager* game;
DisplayManager* displayManager;

void setup() {
  initDebug(9600);
  input = new Input();
  randomiser = new Randomiser(analogRead(0));
  displayManager = new DisplayManager();
  game = new GameManager(input, randomiser, displayManager);
}

void loop() { game->loop(); }
